# README #

This package is used to send control commands to a JACO robotic arm from a unity enviromnent. The conection is established using rosbrigde

## Requirements ##

####  Install MoveIt! For detailed instructions see [MoveIt install page.](http://moveit.ros.org/install/)


```
#!c++

sudo apt-get install ros-indigo-moveit
```


follow Installation instructions, specially for usb permission, copy the udev rule file 10-kinova-arm.rules from ~/catkin_ws/src/kinova-ros/kinova_driver/udev to /etc/udev/rules.d/:

```
#!c++


sudo cp kinova_driver/udev/10-kinova-arm.rules /etc/udev/rules.d/
```



####  Install Trac_IK see [Trac_IK repository.](https://bitbucket.org/traclabs/trac_ik)


```
#!c++

sudo apt-get install ros-indigo-trac-ik
```


####  Install ros controllers see [ros controllers page.](http://wiki.ros.org/ros_controllers)


```
#!c++

 sudo apt-get install ros-indigo-ros-controllers
```


#### Checkout kinova-ros ROS drivers and configuration for JACO arm (kinova ros package) in your catkin workspace


```
#!c++

git clone https://github.com/Kinovarobotics/kinova-ros
```


####  Install the rosbridge_suite see [Rosbrigde page.](http://wiki.ros.org/rosbridge_suite)


```
#!c++

sudo apt-get install ros-indigo-rosbridge-suite
```


#### Compile workspace


```
#!c++

 cd catkin_ws
 catkin_make
```


## Process to launch

#### Launch kinova driver


```
#!c++

 roslaunch kinova_bringup kinova_robot.launch	
```


#### Launch moveit!


```
#!c++

 roslaunch j2n6s300_moveit_config j2n6s300_demo.launch
```


Once RVIZ is open, check the **Allow External Comm.** checkbox, in the planning tab of RVIZ

![allow.jpeg](https://bitbucket.org/repo/MrnBEGM/images/2180640199-allow.jpeg)

#### Execute the publish goals node


```
#!c++

rosrun publish_goals move_jaco.py
```


#### Launch RosBrigde to allow external communication from unity

 
```
#!c++

roslaunch rosbridge_server rosbridge_websocket.launch 
```


### Enjoy