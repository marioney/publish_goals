#! /usr/bin/env python
# ********************************************************************
# Software License Agreement (BSD License)
#
#  Copyright (c) 2014, JSK, The University of Tokyo.
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#   * Neither the name of the JSK, The University of Tokyo nor the names of its
#     nor the names of its contributors may be
#     used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.
# ********************************************************************/

#   Author: Ryohei Ueda, Dave Coleman, Mario Garzon
#   Desc:   Interface between Unity and MoveIt! Motion Planning Rviz Plugin


import threading
from moveit_ros_planning_interface._moveit_robot_interface import RobotInterface
import moveit_commander


import rospy
import time
import tf
from std_msgs.msg import Empty, String
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Pose
from visualization_msgs.msg import InteractiveMarkerInit


def signedSquare(val):
    if val > 0:
        sign = 1
    else:
        sign = -1
    return val * val * sign


class StatusHistory:
    def __init__(self, max_length=10):
        self.max_length = max_length
        self.buffer = []

    def add(self, pose):
        self.buffer.append(pose)
        if len(self.buffer) > self.max_length:
            self.buffer = self.buffer[1:self.max_length + 1]

    def all(self, proc):
        for pose in self.buffer:
            if not proc(pose):
                return False
        return True

    def latest(self):
        if len(self.buffer) > 0:
            return self.buffer[-1]
        else:
            return None

    def length(self):
        return len(self.buffer)

    def clear_all(self):
        for pose in self.buffer:
            del pose
        self.buffer = []

    def new(self, pose, attr):
        if len(self.buffer) == 0:
            return getattr(pose, attr)
        else:
            return getattr(pose, attr) and not getattr(self.latest(), attr)


class MoveJaco:
    def parseSRDF(self):
        ri = RobotInterface("/robot_description")
        planning_groups = {}
        for g in ri.get_group_names():
            self.planning_groups_tips[g] = ri.get_group_joint_tips(g)
            planning_groups[g] = ["/rviz/moveit/move_marker/goal_" + l
                                  for l in self.planning_groups_tips[g]]
        for name in planning_groups.keys():
            if len(planning_groups[name]) == 0:
                del planning_groups[name]
            else:
                print name, planning_groups[name]
        self.planning_groups = planning_groups
        self.planning_groups_keys = planning_groups.keys()  # we'd like to store the 'order'
        self.frame_id = ri.get_planning_frame()

    def __init__(self):
        self.initial_poses = {}
        self.planning_groups_tips = {}
        self.tf_listener = tf.TransformListener()
        self.marker_lock = threading.Lock()
        self.prev_time = rospy.Time.now()
        self.counter = 0
        self.history = StatusHistory(max_length=2)
        self.pre_pose = PoseStamped()
        self.pre_pose.pose.orientation.w = 1
        self.current_planning_group_index = 0
        self.current_eef_index = 0
        self.initialize_poses = False
        self.initialized = False
        self.parseSRDF()
        self.plan_group_pub = rospy.Publisher('/rviz/moveit/select_planning_group', String, queue_size=5)
        self.updatePlanningGroup(0)
        self.updatePoseTopic(0, False)
        self.joy_pose_pub = rospy.Publisher("/joy_pose", PoseStamped, queue_size=1)
        self.plan_pub = rospy.Publisher("/rviz/moveit/plan", Empty, queue_size=5)
        self.execute_pub = rospy.Publisher("/rviz/moveit/execute", Empty, queue_size=5)
        self.update_start_state_pub = rospy.Publisher("/rviz/moveit/update_start_state", Empty, queue_size=5)
        self.update_goal_state_pub = rospy.Publisher("/rviz/moveit/update_goal_state", Empty, queue_size=5)
        self.interactive_marker_sub = rospy.Subscriber(
            "/rviz_moveit_motion_planning_display/robot_interaction_interactive_marker_topic/update_full",
            InteractiveMarkerInit,
            self.markerCB, queue_size=1)
        self.sub = rospy.Subscriber("/unity_pose", Pose, self.PoseCB, queue_size=1)

        scene = moveit_commander.PlanningSceneInterface()
        robot = moveit_commander.RobotCommander()

        rospy.sleep(2.0)
        p = PoseStamped()
        p.header.frame_id = robot.get_planning_frame()
        p.pose.position.x = 0.0
        p.pose.position.y = 0.0
        p.pose.position.z = -0.5
        scene.add_box("table", p, (3.0, 3.0, 1.0))

    def updatePlanningGroup(self, next_index):
        if next_index >= len(self.planning_groups_keys):
            self.current_planning_group_index = 0
        elif next_index < 0:
            self.current_planning_group_index = len(self.planning_groups_keys) - 1
        else:
            self.current_planning_group_index = next_index
        next_planning_group = None
        try:
            next_planning_group = self.planning_groups_keys[self.current_planning_group_index]
        except IndexError:
            msg = 'Check if you started movegroups. Exiting.'
            rospy.logfatal(msg)
            raise rospy.ROSInitException(msg)
        rospy.loginfo("Changed planning group to " + next_planning_group)
        self.plan_group_pub.publish(next_planning_group)

    def updatePoseTopic(self, next_index, wait=True):
        planning_group = self.planning_groups_keys[self.current_planning_group_index]
        topics = self.planning_groups[planning_group]
        if next_index >= len(topics):
            self.current_eef_index = 0
        elif next_index < 0:
            self.current_eef_index = len(topics) - 1
        else:
            self.current_eef_index = next_index
        next_topic = topics[self.current_eef_index]

        rospy.loginfo(
            "Changed controlled end effector to " + self.planning_groups_tips[planning_group][self.current_eef_index])
        self.pose_pub = rospy.Publisher(next_topic, PoseStamped, queue_size=5)
        if wait:
            self.waitForInitialPose(next_topic)
        self.current_pose_topic = next_topic

    def markerCB(self, msg):
        try:
            self.marker_lock.acquire()
            if not self.initialize_poses:
                return
            self.initial_poses = {}
            for marker in msg.markers:
                if marker.name.startswith("EE:goal_"):
                    # resolve tf
                    if marker.header.frame_id != self.frame_id:
                        ps = PoseStamped(header=marker.header, pose=marker.pose)
                        try:
                            transformed_pose = self.tf_listener.transformPose(self.frame_id, ps)
                            self.initial_poses[marker.name[3:]] = transformed_pose.pose
                        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException, e):
                            rospy.logerr("tf error when resolving tf: %s" % e)
                    else:
                        self.initial_poses[marker.name[3:]] = marker.pose  # tf should be resolved
        finally:
            self.marker_lock.release()

    def waitForInitialPose(self, next_topic, timeout=None):
        counter = 0
        while not rospy.is_shutdown():
            counter += 1
            if timeout and counter >= timeout:
                return False
            try:
                self.marker_lock.acquire()
                self.initialize_poses = True
                topic_suffix = next_topic.split("/")[-1]
                if self.initial_poses.has_key(topic_suffix):
                    self.pre_pose = PoseStamped(pose=self.initial_poses[topic_suffix])
                    self.initialize_poses = False
                    return True
                else:
                    rospy.logdebug(self.initial_poses.keys())
                    rospy.loginfo("Waiting for pose topic of '%s' to be initialized",
                                  topic_suffix)
                    rospy.sleep(1)
            finally:
                self.marker_lock.release()

    def PoseCB(self, msg):

        """

        :type msg: Pose
        """
        new_pose = Pose()

        new_pose.orientation = msg.orientation
        new_pose.position = msg.position

        self.run(new_pose)
        self.history.add(new_pose)

    def computePoseFromMsg(self, pre_pose, pose_rec):
        new_pose = PoseStamped()
        new_pose.header.frame_id = self.frame_id
        new_pose.header.stamp = rospy.Time(0.0)
        # move in local
        # if self.history.length() == 1:
        #     x_diff = 0.0
        #     y_diff = 0.0
        #     z_diff = 0.0
        #     self.history.add(pose_rec)
        # else:
        #     x_diff = pose_rec.position.x - self.history.latest().position.x
        #     y_diff = pose_rec.position.y - self.history.latest().position.y
        #     z_diff = pose_rec.position.z - self.history.latest().position.z
        #
        # # rospy.loginfo("x : %f - y : %f -  z : %f, h : %d", x_diff, y_diff, z_diff, self.history.length())
        #
        # new_pose.pose.position.x = pre_pose.pose.position.x + x_diff
        # new_pose.pose.position.y = pre_pose.pose.position.y + y_diff
        # new_pose.pose.position.z = pre_pose.pose.position.z + z_diff

        new_pose.pose.position.x = pose_rec.position.x
        new_pose.pose.position.y = pose_rec.position.y
        new_pose.pose.position.z = pose_rec.position.z - 0.8

        new_pose.pose.orientation.x = pose_rec.orientation.x
        new_pose.pose.orientation.y = pose_rec.orientation.y
        new_pose.pose.orientation.z = pose_rec.orientation.z
        new_pose.pose.orientation.w = pose_rec.orientation.w
        return new_pose

    def run(self, pose):
        if not self.initialized:
            # when not initialized, we will force to change planning_group
            while True:
                self.updatePlanningGroup(self.current_planning_group_index)
                planning_group = self.planning_groups_keys[self.current_planning_group_index]
                topics = self.planning_groups[planning_group]
                next_topic = topics[self.current_eef_index]
                if not self.waitForInitialPose(next_topic, timeout=3):
                    rospy.logwarn("Unable to initialize planning group " + planning_group + ". Trying different group.")
                    rospy.logwarn("Is 'Allow External Comm.' enabled in Rviz? Is the 'Query Goal State' robot enabled?")
                else:
                    rospy.loginfo("Initialized planning group")
                    self.initialized = True
                    self.updatePoseTopic(self.current_eef_index)
                    return
                # Try to initialize with different planning group
                self.current_planning_group_index += 1
                if self.current_planning_group_index >= len(self.planning_groups_keys):
                    self.current_planning_group_index = 0  # reset loop
        # To do: change this by a different topic
        # if self.history.new(pose, "square"):  # plan
        #     rospy.loginfo("Plan")
        #     self.plan_pub.publish(Empty())
        #     return
        # elif self.history.new(pose, "circle"):  # execute
        #     rospy.loginfo("Execute")
        #     self.execute_pub.publish(Empty())
        #     return
        #
        self.marker_lock.acquire()
        pre_pose = self.pre_pose
        new_pose = self.computePoseFromMsg(pre_pose, pose)
        now = rospy.Time.from_sec(time.time())
        # placement.time_from_start = now - self.prev_time
        if (now - self.prev_time).to_sec() > 1 / 10.0:
            rospy.loginfo(new_pose)
            self.pose_pub.publish(new_pose)
            self.joy_pose_pub.publish(new_pose)
            self.prev_time = now
        # sync start state to the real robot state
        self.counter += 1
        # if self.counter % 10:
        self.update_start_state_pub.publish(Empty())
        # if self.counter % 10 == 0:
        rospy.loginfo("Plan - counter %d", self.counter)
        self.plan_pub.publish(Empty())
        rospy.sleep(0.5)
        # if (self.counter + 5) % 10 == 0:
        rospy.loginfo("Execute - counter %d", self.counter)
        self.execute_pub.publish(Empty())
        # self.initialized = False
        self.history.clear_all()
        self.pre_pose = new_pose
        # self.updatePoseTopic(self.current_eef_index + 1)
        self.marker_lock.release()
        # update self.initial_poses
        self.marker_lock.acquire()
        self.initial_poses[self.current_pose_topic.split("/")[-1]] = new_pose.pose
        self.marker_lock.release()
